Install Steamlink and a Push Button handler service

# Installation

Connect the push button to pins 5 (GPIO3) and 6 (GND)
Run the installation script

``` bash
./install.sh
```

It's done, by pressing the push button for a short time it will pair to any PS4 controller nearby in pairing mode and by pressing the push button for 3 seconds or more it will restart the steamlink service.