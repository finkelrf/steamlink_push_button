sudo apt install git python3-pip -y

#clone and install pi_button
git clone https://gitlab.com/finkelrf/pi_button.git
sudo pip3 install pi_button/

#clone and install bluetoothctlwrapper
git clone https://gitlab.com/finkelrf/bluetoothctl-python-wrapper.git
sudo pip3 install bluetoothctl-python-wrapper/

#install steamlink 
sudo apt install steamlink
sudo cp steamlink.service /etc/systemd/system/steamlink.service
sudo chmod 644 /etc/systemd/system/steamlink.service

#install pi_button service
sudo cp push_button.py /usr/bin/push_button.py
sudo cp push_button.service /etc/systemd/system/push_button.service
sudo chmod 644 /etc/systemd/system/push_button.service
sudo systemctl start push_button.service
sudo systemctl enable push_button.service
