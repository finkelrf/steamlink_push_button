#!/usr/bin/env python3

import subprocess
from bluetoothctlwrapper import Bt
from pi_button import PiButton

def connect_to_device(device_name):
    subprocess.call(['sudo','systemctl','restart','bluetooth.service'])
    # device_address = 'A4:AE:11:44:A1:95'
    bt = Bt()
    bt.remove_devices(device_name)
    dev_addr = bt.scan_for_device(device_name)
    if dev_addr:
        bt.connect(dev_addr)
        bt.trust(dev_addr)
    bt.exit()

def connect_to_ps4_controller(button_pressed):
    device_name = 'Wireless Controller'
    connect_to_device(device_name)

def restart_steamlink(button_pressed):
    subprocess.call(['sudo','systemctl','restart','steamlink'])

if __name__ == "__main__":    
    pb = PiButton(3)
    pb.set_short_press_callback(connect_to_device)
    pb.set_long_press_callback(restart_steamlink)

    pb.watch_button()